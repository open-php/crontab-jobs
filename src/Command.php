<?php

namespace Zx\CrontabJobs;

abstract class  Command
{

    abstract public function handle();

    abstract public function getRetry();

    abstract public function getCrontab();

}
