<?php

namespace Zx\CrontabJobs;

use Cron\CronExpression;
use Zx\CrontabJobs\Command;
use \Exception;
use \DateTimeZone;
use \DateTime;

class Kernel
{

    private static array $task = [];

    public static function register(Command $command)
    {
        self::$task[] = $command;
    }

    public static function run($datetime = 'now', DateTimeZone $timezone = null)
    {
        if (empty(self::$task)) {
            return false;
        }
        //contrab目前只能精确到分钟
        $dt = new DateTime($datetime, $timezone);

        foreach (self::$task as $item) {

            if (empty($item->getCrontab()) && !($item instanceof Command)) {
                //如果没有设置crontab就不执行
                continue;
            }
            $expression = new CronExpression($item->getCrontab(), $timezone);
            $isMatching = $expression->isMatching($dt);
            if ($isMatching) {
                //执行具体业务
                try {
                    $item->handle();
                } catch (Exception $e) {
                    //如果执行失败，写入临时重试队列文件，在全部脚本执行完毕之后，在执行重试文件队列
                    self::writeRetryTask($item);
                }
            }
        }
        self::doRetry();
    }

    protected static function writeRetryTask(Command $command)
    {

    }

    protected static function doRetry()
    {

    }
}
