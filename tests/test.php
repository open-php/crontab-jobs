<?php

include_once '../src/Kernel.php';
include_once '../src/Log.php';
include_once '../vendor/autoload.php';

use Zx\CrontabJobs\Command;
use Zx\CrontabJobs\Kernel;
use Cron\CronExpression;

ini_set('date.timezone', 'Asia/Shanghai');

class t1 extends Command
{
    private string $contab;
    private string $retry;

    public function __construct(string $contab = '', int $retry = 0)
    {
        $this->contab = $contab;
        $this->retry = $retry;
    }

    public function handle()
    {
        print_r(self::class);
        echo PHP_EOL;
        print_r(date('Y-m-d H:i:s'));
        echo PHP_EOL;
        print_r('object id->' . spl_object_id($this));
        echo PHP_EOL;
        print_r('object hash->' . spl_object_hash($this));
        echo PHP_EOL;
        echo PHP_EOL;
    }

    public function getRetry()
    {
        return $this->retry;
    }

    public function getCrontab()
    {
        return $this->contab;
    }
}

class t2 extends Command
{
    private string $contab;
    private string $retry;

    public function __construct(string $contab = '', int $retry = 0)
    {
        $this->contab = $contab;
        $this->retry = $retry;
    }

    public function handle()
    {
        print_r(self::class);
        echo PHP_EOL;
        print_r(date('Y-m-d H:i:s'));
        echo PHP_EOL;
        print_r('object id->' . spl_object_id($this));
        echo PHP_EOL;
        print_r('object hash->' . spl_object_hash($this));
        echo PHP_EOL;
        echo PHP_EOL;
    }

    public function getRetry()
    {
        return $this->retry;
    }


    public function getCrontab()
    {
        return $this->contab;
    }
}

$t1 = new t1('* * * * *', 0);
$t2 = new t2('* * * * *', 0);
$t3 = new t1('*/5 * * * *', 0);
$t4 = new t2('*/5 * * * *', 0);

Kernel::register($t1);
Kernel::register($t2);
Kernel::register($t3);
Kernel::register($t4);

Kernel::run('now', new DateTimeZone('Asia/Shanghai'));

//print_r(spl_object_hash($t1));
//echo PHP_EOL;
//print_r(spl_object_id($t1));
//echo PHP_EOL;
//
//print_r(spl_object_hash($t2));
//echo PHP_EOL;
//print_r(spl_object_id($t2));
//echo PHP_EOL;
//
//print_r(spl_object_hash($t3));
//echo PHP_EOL;
//print_r(spl_object_id($t3));
//echo PHP_EOL;
//
//print_r(spl_object_hash($t4));
//echo PHP_EOL;
//print_r(spl_object_id($t4));
//echo PHP_EOL;
//$z =  new DateTimeZone('Asia/Shanghai');
//
//print_r($z->getName());
//print_r($z->getLocation());
//print_r($z->getTransitions());
//print_r($z->getOffset());

//$dt = new DateTime('now',new DateTimeZone('Asia/Shanghai'));
//
//print_r($dt->getTimestamp());
//echo PHP_EOL;
//print_r(time());
//echo PHP_EOL;
//print_r(date('Y-m-d H:i:s',$dt->getTimestamp()));
